function sayHi(){
    var phrase = "Hello";
    console.log(phrase); 
    //phrase อยู่ภายใน function สามารถเรียกทำงานได้
}

sayHi();

//console.log(phrase);
// phrase อยู่ภายใน function สามารถเรียกทำงานไม่ได้
// เพราะว่า var จะใช้ได้แค่ variable ภายในที่ปีกกาครอบอยู่เท่านั้น
